<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PrestaShopWebservice;
use PrestaShopWebserviceException;



class ProductController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url='http://localhost:8080/';
        $key='JIF2YQTVI8GY9N73J1JJU4MZ42KR6UJ5';
        $debug=false;
        try {
            // creating webservice access
            $webService = new PrestaShopWebservice($url, $key, $debug);
            // call to retrieve the blank schema
            $blankXml = $webService->get(['url' => 'http://localhost:8080/api/products?schema=blank']);
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'description' => 'required|string|max:1000|',
                'price' => 'required|int|',
                'id_category_default'=>'required|int|max:3'
            ]);
            if ($validator->fails())
            {
                return response(['errors'=>$validator->errors()->all()], 422);
            }
            $productXML = $blankXml->product[0];
            $productXML->name=$request->input('name');
            $productXML->description = $request->input('description');
            $productXML->price = $request->input('price');
            $productXML->id_category_default = $request->input('id_category_default');
            $productXML->state = 1;
            $createdXml = $webService->add([
                'resource' => 'products',
                'postXml' => $blankXml->asXML(),
            ]);
            $newProductFields = $createdXml->product->children();
            echo 'Product created with ID ' . $newProductFields->id . PHP_EOL;

        }catch
        (PrestaShopWebserviceException $ex) {
            // Shows a message related to the error
            echo 'Other error: <br />' . $ex->getMessage();
        }
    }




}

