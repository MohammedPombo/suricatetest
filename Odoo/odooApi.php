<?php
require_once('./vendor/autoload.php');
require_once('ripcord/ripcord.php'); 

//INITIALISATION DE LA BD ODOO
$url = "http://192.168.43.147:8069/";  // 
$db = 'SuricateTest';  // database name in Odoo 
$username = 'admin';  // Odoo user 
$password = 'admin';  // password for user Odoo or api key 1edca9a7f25fff43635e9c224e5828fb2fb5db2f


$common = ripcord::client("$url/xmlrpc/2/common");  // Connexion
$ver=$common->version();
//print_r($ver);
$uid = $common->authenticate($db, $username, $password, array()); // 

$models = ripcord::client("$url/xmlrpc/2/object"); //RECUPERATION DES MODELES DE LA BD ODOO 

//INITIALISATION DU WEBSERVICE DE PRESTASHOP 
$webService = new PrestaShopWebservice('http://localhost:8080/', 'JIF2YQTVI8GY9N73J1JJU4MZ42KR6UJ5', false);
$xml = $webService->get(['resource' => 'products']); //RECUPERATION DU MODELE PRODUCT FORMAT XML 


$resources = $xml->products->children();

//CREATION D'UN TABLEAU D ID PRODUIT
$tabIdProduct=[];
foreach($resources as $resource ){
    $tabIdProduct[]=$resource['id'];
}
/*
//AFFICHAGE DES ID 
foreach($tabIdProduct as $Idproduct){
    print($Idproduct . PHP_EOL);
}
*/

//CREATION D' UN TABLEAU DE PRODUIT clé: NAME et PRICE
$tabProduct=array();

foreach($tabIdProduct as $Idproduct){
    $xmlDetail = $webService->get([
        'resource' => 'products',
        'id' => $Idproduct, 
    ]);
    $productFields=$xmlDetail->product->children();
    $name=$productFields->name->language;
    $price = $productFields->price;
    $tabProduct[]=['name'=>strval($name),'price'=>floatval($price)];
    
}
print("AFFICHAGE DES  PRODUITS CREES:". PHP_EOL);

foreach($tabProduct as $product){
    print('nom: '.$product['name'].' prix: '.$product['price']. PHP_EOL);
    $id =$models->execute_kw($db, $uid, $password, 'product.product', 'create',array(array('name'=>$product['name'],'price'=>$product['price'])));
    print(PHP_EOL.'id: '.$id . PHP_EOL);

}

?>