<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'da16c01240c09667993ae1b41a89cae0e7604849',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'da16c01240c09667993ae1b41a89cae0e7604849',
            'dev_requirement' => false,
        ),
        'prestashop/prestashop-webservice-lib' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../prestashop/prestashop-webservice-lib',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => 'b58cc0d3d745e6d9df2d892b81fde64872a10742',
            'dev_requirement' => false,
        ),
    ),
);
